# tzdtcontainer-在手机上运行GNU/Linux容器  
# 重要信息！
本项目停止维护！因为各种原因，维护本项目已无任何意义，并且有很多比此项目好的作品，在加上各种不可抗力因素，导致无法进行维护  
在Android使用容器请使用Termux官方的proot-distro  
mt管理器因为targetSDKVersion的更新，已无法伪Termux  

---
本项目基于<del>bash shell语言编写</del>（说这个好像没什么用），有界面交互和命令行交互两种方式使用脚本  
思路来源于项目tmoe-linux（gitee.com/mo2/linux）  
## 使用容器  
在终端模拟器执行以下命令可以启动本脚本  
仅限Termux和mt管理器的终端模拟器可使用容器系统  
启动命令：  
```bash
# 下载脚本
curl -Lo tzdtcontainer https://gitee.com/tzdtwsj/tzdtcontainer/raw/master/tzdtcontainer
# 给予脚本执行权限
chmod +x tzdtcontainer
# 打开脚本界面管理容器
./tzdtcontainer

# 使用命令管理容器
# 具体用法请查看帮助
./tzdtcontainer --help

# 示例
# 安装容器
# 命令语法：./tzdtcontainer install <发行版> <版本> <容器名> [架构（可缺省，不输入默认为系统架构）] [nostart|nochprofile]
# 比方说我要安装一个ubuntu22.04版本，aarch64架构，容器名叫ubuntu-22.04那么可以输入
./tzdtcontainer install ubuntu jammy ubuntu-22.04 aarch64
# 如果只是安装但安装后不自动启动可以在命令后加上nostart
# 如果不想进行容器优化可以在命令后面加上nochprofile

# 删除容器
./tzdtcontainer remove ubuntu-22.04
# 删除容器后必须重启终端，防止出现问题

# 查看容器列表
./tzdtcontainer list

# 杀死容器
# 暂时无出现误杀现象
./tzdtcontainer kill ubuntu-22.04
# 杀死容器后必须重启终端，防止出现问题
```  
## 提示
脚本支持多架构了！支持armhf，aarch64，i386，x86\_64这四种Android系统架构  
不支持root用户执行本脚本  
仅支持Android7及以上系统  

---
## 脚本功能  
安装/启动GNU/Linux容器（支持跨架构）  
无root伪root  
mt终端伪termux(直接安装termux不就完事了吗？)  
后续可能还会更新新功能的（也许吧......)

#!/system/bin/sh
Install_git(){
	install_jg=$(cat $HOME/.local/tzdtcontainer/install_jg)
	case $install_jg in
		aarch64)install_git=aarch64
			;;
		arm)install_git=arm
			;;
		i386)install_git=i686
			;;
		x86_64)install_git=x86_64
	esac
	REPO_URL="https://mirrors.tuna.tsinghua.edu.cn/termux/apt/termux-main/pool/main"
	PACKAGE_URL="g/git l/less libc/libcurl libi/libiconv libn/libnghttp2 libs/libssh2 o/openssl p/pcre2 z/zlib"
	for i in $PACKAGE_URL; do
		PACKAGES_URL="$PACKAGES_URL $REPO_URL/$i/$(curl --connect-timeout 7 -L $REPO_URL/$i | grep '\.deb' | grep "${install_git}" | sed -E 's@<a (href)@\n\1@g' | awk -F 'href=' '{print $2}' | cut -d '"' -f 2 | grep deb | tail -n 1)"
		#PACKAGES_NAME="$PACKAGES_NAME $(curl --connect-timeout 7 -L ${REPO_URL}/$i | grep '\.deb' | grep "${install_git}" | sed -E 's@<a (href)@\n\1@g' | awk -F 'href=' '{print $2}' | cut -d '"' -f 2 | grep deb | tail -n 1)"
	done
	unset i
	PACKAGES_URL="$PACKAGES_URL $REPO_URL/c/ca-certificates/$(curl --connect-timeout 7 -L $REPO_URL/c/ca-certificates | grep '\.deb' | grep "all" | sed -E 's@<a (href)@\n\1@g' | awk -F 'href=' '{print $2}' | cut -d '"' -f 2 | grep deb | tail -n 1)"
	for i in $PACKAGES_URL; do
		wget $i -O $HOME/.local/tzdtcontainer/git-depends.deb
		if test "$(command -v dpkg-deb)" = ""; then
			"$HOME/.local/tzdtcontainer/proot_prefix/bin/proot" --mount=$HOME/.local/tzdtcontainer/proot_prefix/bin/tar:$PREFIX/bin/tar /system/bin/env LD_LIBRARY_PATH="$HOME/.local/tzdtcontainer/proot_prefix/lib:$PREFIX/lib" "$HOME/.local/tzdtcontainer/proot_prefix/bin/dpkg-deb" -X $HOME/.local/tzdtcontainer/git-depends.deb $HOME/.local/tzdtcontainer/git_prefix
		else
			dpkg-deb -X $HOME/.local/tzdtcontainer/git-depends.deb $HOME/.local/tzdtcontainer/git_prefix
		fi
		
	done
	rm $HOME/.local/tzdtcontainer/git-depends.deb
	"$HOME/.local/tzdtcontainer/proot_prefix/bin/proot" -r $HOME/.local/tzdtcontainer/git_prefix -m /linkerconfig/ld.config.txt:/linkerconfig/ld.config.txt -m $HOME/.local/tzdtcontainer/git_prefix/data/data/com.termux/files/usr:/data/data/com.termux/files/usr -m /system:/system -m /apex:/apex -m /proc:/proc -m /dev:/dev --pwd=/ /system/bin/env LD_LIBRARY_PATH= PATH=/data/data/com.termux/files/usr/bin git --version >/dev/null
	if test "$?" != "0"; then
		echo 安装git工具失败
		exit 1
	fi
	echo 安装成功!
	exit 0
}
Install_git
